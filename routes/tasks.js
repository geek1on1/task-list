const express = require('express');
const router = express.Router();
const mongojs = require('mongojs');
const database = mongojs('mongodb://geek1on1:*awe2000@ds231941.mlab.com:31941/task_list', ['tasks']);

/**
 * Requests all tasks
 */
router.get('/tasks', (req, res, next) => {
    database.tasks.find((error, tasks) => {
        if(error) res.send(error);
        res.json(tasks);
    });
});

/**
 * Requests one single task
 */
router.get('/task/:id', (req, res, next) => {
    database.tasks.findOne({_id: mongojs.ObjectID(req.params.id)}, (error, task) => {
        if(error) res.send(error);
        res.json(task);
    });
});

module.exports = router;